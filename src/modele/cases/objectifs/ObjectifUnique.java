package modele.cases.objectifs;

import modele.Jeu;
import modele.entites.blocs.BlocObjectif;

public class ObjectifUnique extends Objectif{
    private final BlocObjectif blocObjectif;

    public ObjectifUnique(Jeu _jeu, BlocObjectif _blocObjectif) {
        super(_jeu);
        blocObjectif = _blocObjectif;
    }

    @Override
    public boolean estValide(){
        return e!=null && this.e.equals(blocObjectif);
    }
}
