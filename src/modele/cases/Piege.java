package modele.cases;
import modele.Jeu;
import modele.entites.Entite;
import modele.entites.blocs.Bloc;

public class Piege extends Vide {
    private boolean active = false;
    private Bloc blocQuiRempli;

    public Piege(Jeu _jeu) {
        super(_jeu);
    }

    public boolean isActive() {
        return this.active;
    }

    public boolean isRempli() {
        return this.blocQuiRempli != null;
    }

    @Override
    public boolean entrerSurLaCase(Entite e) {
        setEntite(e);
        if (active) {
            if (blocQuiRempli != null) {
                setEntite(e);
            } else if (e instanceof Bloc) {
                System.out.println("Le bloc est tombé dans le piège");
                blocQuiRempli = (Bloc) e;
                super.quitterLaCase();

            } else {
                System.out.println("Hero mort !");
            }
        }
        jeu.update();
        return true;
    }

    @Override
    public boolean quitterLaCase() {
        if (!active) {
            active = true;
        } else if (blocQuiRempli == null) {
            return false;
        }
        e = null;
        return true;
    }
}