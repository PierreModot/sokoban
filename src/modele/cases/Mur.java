package modele.cases;

import modele.Jeu;
import modele.entites.Entite;

public class Mur extends Case {
    public Mur(Jeu _jeu) { super(_jeu); }

    @Override
    public boolean peutEtreParcouru(Entite e) {
        return false;
    }
}
