package modele.cases;

import modele.Direction;
import modele.Jeu;
import modele.entites.Entite;

public class TapisRoulant extends Vide{
    private Direction direction;

    public TapisRoulant(Jeu _jeu, Direction _direction) {
        super(_jeu);
        direction = _direction;
    }

    @Override
    public boolean entrerSurLaCase(Entite e) {
        setEntite(e);
        jeu.update();

        try {
            Thread.sleep(100);
        } catch (InterruptedException ex) {
            throw new RuntimeException(ex);
        }

        jeu.deplacerEntite(e, direction);

        return true;
    }

    public Direction getDirection() {
        return direction;
    }
}
