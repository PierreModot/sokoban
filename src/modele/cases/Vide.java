package modele.cases;

import modele.Jeu;
import modele.entites.Entite;

public class Vide extends Case {

    public Vide(Jeu _jeu) { super(_jeu); }

    @Override
    public boolean peutEtreParcouru(Entite e) {
        return this.e == null;
    }



}
