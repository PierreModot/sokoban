package modele.entites.blocs;

import modele.cases.Case;
import modele.Couleurs;
import modele.Jeu;

public class BlocObjectifColore extends BlocObjectif{

    private Couleurs couleur;
    public BlocObjectifColore(Jeu _jeu, Case c, Couleurs _couleur) {
        super(_jeu, c);
        couleur = _couleur;
    }

    public Couleurs getCouleur(){return couleur;}
}
