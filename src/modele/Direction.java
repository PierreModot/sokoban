/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele;

/** Type énuméré des directions : les directions correspondent à un ensemble borné de valeurs, connu à l'avance
 *
 *
 */
public enum Direction {
    haut, bas, gauche, droite;

    public static Direction inverse(Direction d){
        Direction retour = null;
        switch (d){
            case haut:
                retour = bas;
                break;
            case droite:
                retour = gauche;
                break;
            case bas:
                retour = haut;
                break;
            case gauche:
                retour = droite;
                break;
        }
        return retour;
    }
}